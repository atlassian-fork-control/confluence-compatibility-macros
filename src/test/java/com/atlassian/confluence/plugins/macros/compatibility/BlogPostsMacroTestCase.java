package com.atlassian.confluence.plugins.macros.compatibility;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.commons.lang.RandomStringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.ContentIncludeStack;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.InvalidDurationException;
import com.opensymphony.util.TextUtils;


public class BlogPostsMacroTestCase extends TestCase
{
    private Map<String, String> macroParameters;
    private final String macroOutput = "Fake macro output";
    private final static String SPACE_KEY = "TST";

    @Mock
    private PageManager pageManager;

    @Mock
    private BlogPostsMacro blogPostsMacro;

    @Mock
    private Page pageToBeRendered;
    
    @Mock
    private ExcerptHelper excerptHelper;
    
    @Mock
    private ConversionContext conversionContext;
    
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        
        macroParameters = new HashMap<String, String>();

        blogPostsMacro = new BlogPostsMacro()
        {
            protected String getText(String key, Object[] substitution)
            {
                return key;
            }
            
            protected Map<String, Object> getMacroVelocityContext()
            {
                return new HashMap<String, Object>();
            }
        };
        setProperties();
        
        pageToBeRendered = new Page();
        pageToBeRendered.setSpace(new Space(SPACE_KEY));
        conversionContext = new DefaultConversionContext(pageToBeRendered.toPageContext());
    }

    protected void tearDown() throws Exception
    {
        while (!ContentIncludeStack.isEmpty())
            ContentIncludeStack.pop();
    	
        pageManager = null;
        blogPostsMacro = null;
        pageToBeRendered = null;
        macroParameters = null;
        excerptHelper = null;
        
        super.tearDown();
    }

    public void testMacroCanOnlyBeRenderedOncePerRenderContext() throws MacroExecutionException
    {
    	blogPostsMacro = new BlogPostsMacro()
        {
    		protected boolean isBeingRendered() 
    	    {
    	        return true;
    	    }
        };
        assertNull(blogPostsMacro.execute(macroParameters, null, conversionContext));
    }

    public void testErrorRenderedWhenPageAlreadyIncluded() throws MacroExecutionException
    {
        ContentIncludeStack.push(pageToBeRendered);
        
        macroParameters.put("content", null);

        assertEquals(
                "<div class=\"error\">blogpostmacro.error.alreadyincludedpage</div>",
                blogPostsMacro.execute(macroParameters, null, conversionContext));
    }

    public void testOnlyLastFifteenBlogsRetrievedIfMaxParameterNotSpecified() throws MacroExecutionException
    {
        final String contentType = "blogpost";
        
        setParameters(contentType, null, null);
        
        when(pageManager.getRecentlyAddedBlogPosts(15, null, SPACE_KEY)).thenReturn(Collections.EMPTY_LIST);
        
        blogPostsMacro = new BlogPostsMacro()
        {
            protected Map<String, Object> getMacroVelocityContext()
            {
                return new HashMap<String, Object>();
            }

            protected String renderBlogs(Map<String, Object> contextMap)
            {
                assertEquals(new ArrayList(), contextMap.get("posts"));
                assertEquals(contentType, contextMap.get("contentType"));

                return macroOutput;
            }
        };
        setProperties();
        assertMacroExecute();
        assertMacroCleanedItselfUp();
    }

    public void testOnlyLastFifteenBlogsRetrievedIfMaxParameterIsNotAnInteger() throws MacroExecutionException
    {
        final String contentType = "blogpost";
        
        setParameters(contentType, "notAnInteger", null);
        
        when(pageManager.getRecentlyAddedBlogPosts(15, null, SPACE_KEY)).thenReturn(Collections.EMPTY_LIST);

        blogPostsMacro = new BlogPostsMacro()
        {
            protected Map<String, Object> getMacroVelocityContext()
            {
                return new HashMap<String, Object>();
            }

            protected String renderBlogs(Map<String, Object> contextMap)
            {
                assertEquals(new ArrayList(), contextMap.get("posts"));
                assertEquals(contentType, contextMap.get("contentType"));

                return macroOutput;
            }
        };

        setProperties();
        assertMacroExecute();
        assertMacroCleanedItselfUp();
    }

    public void testBlogsRetrievedBasedOnTimeParameter() throws MacroExecutionException, InvalidDurationException
    {
        final String contentType = "blogpost";
        final String timeSpan = "1d";
        Date timeSince = null;
        
        timeSince = new Date(new Date().getTime() - DateUtils.getDuration(timeSpan) * 1000);
        setParameters(contentType, null, timeSpan);
        
        when(pageManager.getRecentlyAddedBlogPosts(0, timeSince, SPACE_KEY)).thenReturn(Collections.EMPTY_LIST);

        blogPostsMacro = new BlogPostsMacro()
        {
            protected Map<String, Object> getMacroVelocityContext()
            {
                return new HashMap<String, Object>();
            }

            protected String renderBlogs(Map<String, Object> contextMap)
            {
                assertEquals(new ArrayList(), contextMap.get("posts"));
                assertEquals(contentType, contextMap.get("contentType"));

                return macroOutput;
            }
        };

        setProperties();
        assertMacroExecute();
        assertMacroCleanedItselfUp();
    }

    public void testOnlyBlogPostTitlesRenderedIfContentIsSetToTitles() throws MacroExecutionException
    {
        final String contentType = BlogPostsMacro.DISPLAY_TITLES_ONLY;
        final BlogPost blogPost1 = new BlogPost();
        final String blogPost1RenderedHtml = "<font size=\"7\">hello!</font>";
        List blogPosts = Arrays.asList(new Object[] { blogPost1 });
        BlogPost post = (BlogPost) blogPosts.iterator().next();
        
        setParameters(contentType, null, null);
        
        when(pageManager.getRecentlyAddedBlogPosts(15, null, SPACE_KEY)).thenReturn(blogPosts);
        
        blogPostsMacro = new BlogPostsMacro()
        {
            protected Map<String, Object> getMacroVelocityContext()
            {
                return new HashMap<String, Object>();
            }

            protected String renderBlogTitles(Map<String, Object> contextMap)
            {
                final List postTuples = (List) contextMap.get("posts");
                final PostHtmlTuple postHtmlTuple;

                assertNotNull(postTuples);
                assertEquals(1, postTuples.size());

                postHtmlTuple = (PostHtmlTuple) postTuples.get(0);
                assertEquals(blogPost1, postHtmlTuple.getPost());
                assertEquals(blogPost1RenderedHtml, postHtmlTuple.getRenderedHtml());

                assertEquals(contentType, contextMap.get("contentType"));

                return macroOutput;
            }
            
            protected String getContent(BlogPost post, String contentType)
            {
            	return blogPost1RenderedHtml;
            }
        };

        setProperties();
        assertMacroExecute();
        assertMacroCleanedItselfUp();
    }

    public void testBlogExcerptsRenderedIfContentIsSetToExcerpts() throws MacroExecutionException
    {
        final String contentType = BlogPostsMacro.DISPLAY_EXCERPTS;
        final BlogPost blogPost1 = new BlogPost();
        final String blogPost1Excerpt = RandomStringUtils.randomAlphanumeric(300);
        final String blogPost1RenderedHtml = "<font size=\"7\">" + blogPost1Excerpt + "</font>";
        List blogPosts = Arrays.asList(new Object[] { blogPost1 });
        BlogPost post = (BlogPost) blogPosts.iterator().next();
        
        setParameters(contentType, null, null);
        
        when(pageManager.getRecentlyAddedBlogPosts(15, null, SPACE_KEY)).thenReturn(blogPosts);
        when(excerptHelper.getExcerpt(blogPost1)).thenReturn(blogPost1RenderedHtml);
        
        blogPostsMacro = new BlogPostsMacro()
        {
            protected Map<String, Object> getMacroVelocityContext()
            {
                return new HashMap<String, Object>();
            }

            protected String renderBlogs(Map contextMap)
            {
                final List postTuples = (List) contextMap.get("posts");
                final PostHtmlTuple postHtmlTuple;

                assertNotNull(postTuples);
                assertEquals(1, postTuples.size());

                postHtmlTuple = (PostHtmlTuple) postTuples.get(0);
                assertEquals(blogPost1, postHtmlTuple.getPost());
                assertEquals(blogPost1RenderedHtml, postHtmlTuple.getRenderedHtml());

                assertEquals(contentType, contextMap.get("contentType"));

                return macroOutput;
            }
        };

        setProperties();
        assertMacroExecute();
        assertMacroCleanedItselfUp();
    }

    public void testBlogContentAsExcerptsRenderedIfContentIsSetToExcerpts() throws MacroExecutionException
    {
        final String contentType = BlogPostsMacro.DISPLAY_EXCERPTS;
        final BlogPost blogPost1 = new BlogPost();
        final String blogPost1Excerpt = RandomStringUtils.randomAlphanumeric(300);
        final String blogPost1RenderedHtml = "<font size=\"7\">" + blogPost1Excerpt + "</font>";
        List blogPosts = Arrays.asList(new Object[] { blogPost1 });
        BlogPost post = (BlogPost) blogPosts.iterator().next();
        blogPost1.setBodyAsString(blogPost1Excerpt);
        
        setParameters(contentType, null, null);
        
        when(pageManager.getRecentlyAddedBlogPosts(15, null, SPACE_KEY)).thenReturn(blogPosts);
        when(excerptHelper.getExcerpt(blogPost1)).thenReturn(blogPost1RenderedHtml);

        blogPostsMacro = new BlogPostsMacro()
        {
            protected Map<String, Object> getMacroVelocityContext()
            {
                return new HashMap<String, Object>();
            }

            protected String renderBlogs(Map<String, Object> contextMap)
            {
                final List postTuples = (List) contextMap.get("posts");
                final PostHtmlTuple postHtmlTuple;

                assertNotNull(postTuples);
                assertEquals(1, postTuples.size());

                postHtmlTuple = (PostHtmlTuple) postTuples.get(0);
                assertEquals(blogPost1, postHtmlTuple.getPost());
                assertEquals(blogPost1RenderedHtml, postHtmlTuple.getRenderedHtml());

                assertEquals(contentType, contextMap.get("contentType"));

                return macroOutput;
            }
        };

        setProperties();
        assertMacroExecute();
        assertMacroCleanedItselfUp();
    } 

    public void testErrorDivsOfBlogPostRendered() throws MacroExecutionException
    {
        final String contentType = BlogPostsMacro.DISPLAY_EXCERPTS;
        final BlogPost blogPost1 = new BlogPost();
        final String blogPost1Excerpt = RandomStringUtils.randomAlphanumeric(300);
        final String blogPost1RenderedHtml = "<div class=\"error\">Fake error</div>";
        List blogPosts = Arrays.asList(new Object[] { blogPost1 });
        BlogPost post = (BlogPost) blogPosts.iterator().next();

        blogPost1.setBodyAsString(blogPost1Excerpt);
        
        setParameters(contentType, null, null);
        
        when(pageManager.getRecentlyAddedBlogPosts(15, null, SPACE_KEY)).thenReturn(blogPosts);
        when(excerptHelper.getExcerpt(blogPost1)).thenReturn(blogPost1RenderedHtml);

        blogPostsMacro = new BlogPostsMacro()
        {
            protected Map<String, Object> getMacroVelocityContext()
            {
                return new HashMap<String, Object>();
            }

            protected String renderBlogs(Map<String, Object> contextMap)
            {
                final List postTuples = (List) contextMap.get("posts");
                final PostHtmlTuple postHtmlTuple;

                assertNotNull(postTuples);
                assertEquals(1, postTuples.size());

                postHtmlTuple = (PostHtmlTuple) postTuples.get(0);
                assertEquals(blogPost1, postHtmlTuple.getPost());
                assertEquals(blogPost1RenderedHtml, postHtmlTuple.getRenderedHtml());

                assertEquals(contentType, contextMap.get("contentType"));

                return macroOutput;
            }
        };

        setProperties();
        assertMacroExecute();
        assertMacroCleanedItselfUp();
    } 

    public void testTrimmedBlogContentAsExcerptsRenderedIfContentIsSetToExcerpts() throws MacroExecutionException
    {
        final String contentType = BlogPostsMacro.DISPLAY_EXCERPTS;
        final BlogPost blogPost1 = new BlogPost();
        final String blogPost1Excerpt = RandomStringUtils.randomAlphanumeric(501); /* Exceeds 500 characters, where the macro should trim it too 500 ellipsed */
        final String blogPost1RenderedHtml = "<font size=\"7\">" + TextUtils.trimToEndingChar(blogPost1.getBodyAsString(), 500) + "...</font>";
        List blogPosts = Arrays.asList(new Object[] { blogPost1 });
        BlogPost post = (BlogPost) blogPosts.iterator().next();

        blogPost1.setBodyAsString(blogPost1Excerpt);
        
        setParameters(contentType, null, null);
        
        when(pageManager.getRecentlyAddedBlogPosts(15, null, SPACE_KEY)).thenReturn(blogPosts);
        when(excerptHelper.getExcerpt(blogPost1)).thenReturn(blogPost1RenderedHtml);

        blogPostsMacro = new BlogPostsMacro()
        {
            protected Map<String, Object> getMacroVelocityContext()
            {
                return new HashMap<String, Object>();
            }

            protected String renderBlogs(Map<String, Object> contextMap)
            {
                final List postTuples = (List) contextMap.get("posts");
                final PostHtmlTuple postHtmlTuple;

                assertNotNull(postTuples);
                assertEquals(1, postTuples.size());

                postHtmlTuple = (PostHtmlTuple) postTuples.get(0);
                assertEquals(blogPost1, postHtmlTuple.getPost());
                assertEquals(blogPost1RenderedHtml, postHtmlTuple.getRenderedHtml());

                assertEquals(contentType, contextMap.get("contentType"));

                return macroOutput;
            }
        };

        setProperties();
        assertMacroExecute();
        assertMacroCleanedItselfUp();
    } 
    
    private void assertMacroExecute() throws MacroExecutionException
    {
    	assertEquals(
                macroOutput,
                blogPostsMacro.execute(macroParameters, null, conversionContext));
    }
    
    private void assertMacroCleanedItselfUp()
    {
    	assertTrue(ContentIncludeStack.isEmpty()); /* Ensure that ever ContentEntityObject is popped off */
    }
    
    private void setProperties()
    {
      blogPostsMacro.setPageManager(pageManager);
      blogPostsMacro.setExcerptHelper(excerptHelper);
    }
    
    private void setParameters(String contentType, String max, String time)
    {
    	macroParameters.put("content", contentType);
        macroParameters.put("max", null);
        macroParameters.put("time", null);
    }
    
}
