package com.atlassian.confluence.plugins.macros.compatibility;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.persistence.ContentEntityObjectDao;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;

/**
 * A macro to display all the content created by one user
 */
public class SnipsByUserMacro extends BaseMacro implements Macro
{
	public static final Logger log = Logger.getLogger(SnipsByUserMacro.class);
	
    private static final String MACR_NAME = "snips-by-user";
    private UserAccessor userAccessor;
    private ContentEntityObjectDao contentEntityObjectDao;
    private PermissionManager permissionManager;

    public String getName()
    {
        return MACR_NAME;
    }

    public void setUserAccessor(UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }
    
    public void setContentEntityObjectDao(ContentEntityObjectDao contentEntityObjectDao)
    {
        this.contentEntityObjectDao = contentEntityObjectDao;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

	public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
	{
		String username = parameters.get("user") == null ? parameters.get("0") : parameters.get("user").toString().trim();

		Map contextMap = MacroUtils.defaultVelocityContext();
		User user = fetchUser(username);
		List content = fetchContent(username);
		Collections.sort(content);

		contextMap.put("user", user);
		contextMap.put("content", content);

		try
		{
			return VelocityUtils.getRenderedTemplate("templates/macros/contentbyuser.vm", contextMap);
		}
		catch (Exception e)
		{
			log.error("Error while trying to assemble the ContentByUser result!", e);
			throw new MacroExecutionException(e);
		}
	}
	
	public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
	{
		try
		{
			return execute(parameters, body, new DefaultConversionContext(renderContext));
		} 
		catch (MacroExecutionException e)
		{
			throw new MacroException(e);
		}
	}
	
	public final boolean hasBody()
    {
        return true;
    }
	
	public final RenderMode getBodyRenderMode()
	{
		return RenderMode.NO_RENDER;
	}
	
	public BodyType getBodyType() 
	{
		return BodyType.NONE;
	}

	public OutputType getOutputType() 
	{
		return OutputType.BLOCK;
	}
	
    @Override
	public TokenType getTokenType(Map parameters, String body, RenderContext context) 
    {
		return TokenType.BLOCK;
	}
	
	private User fetchUser(String username)
	{
		return userAccessor.getUser(username);
	}
	
	private List fetchContent(String username)
	{
		List result = contentEntityObjectDao.getContentAuthoredByUser(username);

		if (result == null || result.isEmpty())
			return Collections.EMPTY_LIST;

		return permissionManager.getPermittedEntities(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, result);
	}
	
}
