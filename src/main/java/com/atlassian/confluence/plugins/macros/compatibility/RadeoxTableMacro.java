package com.atlassian.confluence.plugins.macros.compatibility;

import org.radeox.macro.TableMacro;

import com.atlassian.confluence.core.ConfluenceActionSupport;

/**
 * For easier compatibility with Radeox tables imported from other applications. This
 * macro should not be recommended or documented: prefer Textile tables.
 */
public class RadeoxTableMacro extends TableMacro 
{
    private static final String MACRO_NAME = "table";

    protected String getText(String key)
    {
        return ConfluenceActionSupport.getTextStatic(key);
    }

    public String getName()
    {
        return MACRO_NAME;
    }

    public String getDescription()
    {
        return getText("radeoxtablemacro.description");
    }

}
