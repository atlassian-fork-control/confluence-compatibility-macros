package com.atlassian.confluence.plugins.macros.compatibility;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;

public class LinkMacroMigrator implements MacroMigration 
{
	private static final String LINK_PARAM_NAME = "url";
	private static final String TEXT_PARAM_NAME = "text";
	
	public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
	{
		final String defaultParam = macroDefinition.getDefaultParameterValue();
		Map<String, String> parameters = macroDefinition.getParameters();
		
		if (parameters == null)
		{
			parameters = new HashMap<String, String>();
		}
		else
		{
			if (parameters.containsKey(TEXT_PARAM_NAME) && parameters.containsKey(LINK_PARAM_NAME)) // {link:text=foobar|url=http://localhost/confluence}
			{
				return macroDefinition;
			}
			else
			{
				if (parameters.size() > 1)
				{
					if (parameters.containsKey(TEXT_PARAM_NAME)) // {link:text=foobar|http://localhost/confluence}
					{
						Set<String> keySet = parameters.keySet();
						for(String key : keySet)
						{
							if (!TEXT_PARAM_NAME.equals(key))
							{
								parameters.put(LINK_PARAM_NAME, parameters.get(key));
								parameters.remove(key);
								break;
							}
						}
					}
					else if (parameters.containsKey(LINK_PARAM_NAME))  // {link:url=http://localhost/confluence|foobar}   parameters={foobar=, url=http://localhost/confluence}]
					{
						Set<String> keySet = parameters.keySet();
						for (String key : keySet)
						{
							if (!LINK_PARAM_NAME.equals(key))
							{
								parameters.put(TEXT_PARAM_NAME, parameters.get(key));
								parameters.remove(key);
								break;
							}
						}
					}
				}
			}
		}
		
		if (StringUtils.isNotBlank(defaultParam))
		{
			if (parameters.containsKey(LINK_PARAM_NAME)) // {link:foobar|url=http://localhost/confluence} 
			{
				parameters.put(TEXT_PARAM_NAME, defaultParam);
				macroDefinition.setParameters(parameters);
				return macroDefinition;
			}
			if (parameters.containsKey(TEXT_PARAM_NAME)) // {link:http://localhost/confluence|text=foobar} 
			{
				parameters.put(LINK_PARAM_NAME, defaultParam);
				macroDefinition.setParameters(parameters);
				return macroDefinition;
			}
			
			// {link:foobar}
			if (parameters.size() == 0)
			{
				parameters.put(TEXT_PARAM_NAME, defaultParam);
				macroDefinition.setParameters(parameters);
				return macroDefinition;
			}
			else // {link:foobar|http://localhost/confluence}
			{
				Set<String> keySet = parameters.keySet();
				for (String key : keySet)
				{
					parameters.put(LINK_PARAM_NAME, parameters.get(key));
					parameters.remove(key);
					break;
				}
				parameters.put(TEXT_PARAM_NAME, defaultParam);
				
			}
			macroDefinition.setParameters(parameters);
		}
		return macroDefinition;
	}
	

}
