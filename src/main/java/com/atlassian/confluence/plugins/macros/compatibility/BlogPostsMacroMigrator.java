package com.atlassian.confluence.plugins.macros.compatibility;


public class BlogPostsMacroMigrator extends CompatibilityMacroMigrator
{
	private static final String BLOGPOST_PARAM_NAME = "max";

	@Override
	public String getDefaultParamName() 
	{
		return BLOGPOST_PARAM_NAME;
	}
	
}
